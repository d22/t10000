<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main">
	<title><g:message code="projectview.page.title"/></title>
</head>

<body>

<div id="projectList">
	<table>
		<thead>
		<tr>
			<th>id</th>
			<th>name</th>
			<th>group</th>
			<th>owner</th>
		</tr>
		</thead>
		<tbody>
		<g:each in="${projectList}" status="i" var="project">
			<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
				<td>${project.id}</td>
				<td><g:remoteLink action="selectProject" params="[id: project.id]" update="projectDetail">${project.name}</g:remoteLink></td>
				<td>${project.group?.name}</td>
				<td>${project.owner.firstname}</td>
			</tr>
		</g:each>
		</tbody>
	</table>
	<div id="projectDetail">
		<tmpl:projectDetailTemlate selectedProject="${selectedProject}" />
	</div>
</div>
</body>
</html>

import t10000.Client
import t10000.ClientContact
import t10000.Payment
import t10000.Project
import t10000.ProjectGroup
import t10000.Task
import t10000.User
import t10000.Work

class BootStrap {

	def init = { servletContext ->
		println "bootstrap!"
		createTestData()
		println "bootstrap done!"
	}

	def destroy = {
	}

	def createTestData() {

		println "createTestData"

		def u = new User(firstname: "Jonas", lastname: "Furrer", nickname: "jonas", emailaddress: "jonas@mail.ru", password: "password")
		u.save(flush: true)

		def c = new Client(name: "Generic Client")
		c.save(flush: true)

		def cc = new ClientContact(name: "contact", client: c)
		cc.save();

		def g = new ProjectGroup(name: "Generic Group")
		g.save()

		def p = new Project()
		p.setGroup(g)
		p.setName("Start Project")
		p.setClient(c)
		p.setOwner(u)
		p.setProjectStart(new Date())
		p.save()

		def p2 = new Project(name: "Second Project", owner: u, group: g)
		p2.save()

		def p3 = new Project(name: "Administration", owner: u)
		p3.save()

		new Payment(project: p, amount: 1000).save()

		def task = new Task(name: "admin", project: p).save()

		new Work(user: u, taks: task).save()

		println "createTestData done"

	}
}

package t10000

class UserController {

	def index(Integer max) {
		params.max = Math.min(max ?: 10, 100)
		respond User.list(params), model: [userInstanceCount: User.count()]
	}


}

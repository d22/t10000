package t10000

class ProjectController {

	def index() {
		List allProjects = Project.list()
		def selectedProject = allProjects.first()
		def model = [projectList: allProjects, selectedProject: selectedProject]
		return model
	}

	def selectProject() {
		def projectId = params.id
		def selectedProject = Project.get(projectId)
		render(template: "projectDetailTemlate", model: [selectedProject: selectedProject])
	}

}

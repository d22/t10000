package t10000

class User {

	static constraints = {
		work(nullable: true)
		projects(nullable: true)
		firstname(blank: false)
		lastname(blank: false)
		nickname(blank: false, minSize: 3)
		emailaddress(blank: false)
		password(blank: true)
	}

	static mapping = {
		table 'usertable'
	}

	static hasMany = [work:Work, projects:Project]

	Date dateCreated
	Date lastUpdated
	String firstname
	String lastname
	String nickname
	String emailaddress
	String password
	Boolean active = true

}

package t10000

class ProjectGroup {

	static constraints = {
		projects(nullable: true)
		name(blank: false)
	}

	static hasMany = [projects:Project]

	Date dateCreated
	Date lastUpdated
	String name

}

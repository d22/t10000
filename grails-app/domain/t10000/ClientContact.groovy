package t10000

class ClientContact {

	static constraints = {
		client(nullable: false)
		name(blank: false)
	}

	static belongsTo = [client:Client]

	Date dateCreated
	Date lastUpdated
	String name
	String firstname = ""
	String lastname = ""
	String email = ""
	String data = ""

}

package t10000

class Payment {

	static constraints = {
		project(nullable: false)
		invoiceDate(nullable: true)
		payDate(nullable: true)
	}

	static belongsTo = [project:Project]

	Date dateCreated
	Date lastUpdated
	double amount = 0
	Date invoiceDate
	Date payDate

}

package t10000

class Task {

	static constraints = {
		project(nullable: false)
		name(blank: false)
	}

	static belongsTo = [project:Project]

	Date dateCreated
	Date lastUpdated
	String name
	double timerate = 0
	boolean billable = true

}

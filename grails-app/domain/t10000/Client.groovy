package t10000

class Client {

	static constraints = {
		clientContacts(nullable: true)
		name(blank: false)
		email(blank: true)
		data(blank: true)
		note(blank: true)
	}

	static hasMany = [clientContacts: ClientContact]

	Date dateCreated
	Date lastUpdated
	String name
	String email = ""
	String data = ""
	String note = ""

}

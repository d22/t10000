package t10000

class Project {

	static constraints = {
		group(nullable: true)
		payments(nullable: true)
		tasks(nullable: true)
		owner(nullable: false)
		name(blank: false)
		client(nullable: true)
		projectStart(nullable: true)
		projectEnd(nullable: true)
		note(blank: true)
	}

	static belongsTo = [group:ProjectGroup]
	static hasMany = [payments:Payment, tasks:Task]
	static hasOne = [owner:User]

	Date dateCreated
	Date lastUpdated
	String name
	Client client
	Date projectStart
	Date projectEnd
	String note = ""
	double timerate = 0
	boolean archived = false

}

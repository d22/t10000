package t10000

class Work {

	static constraints = {
		task(nullable: false)
		user(nullable: false)
	}

	static belongsTo = [task:Task,user:User]

	Date workStart
	Date workEnd

}
